<?php
	 session_start();
	 $conn = mysqli_connect("127.0.0.1","root","","dbhdwl");
	 if (!$conn) 
	 {
		 die("Connection failed: " . mysqli_connect_error(). mysqli_connect_errno() . PHP_EOL);
	 }
	 if(isset($_SESSION["emp"]))
	 {
		 
	 }
	 elseif(isset($_SESSION["mm"]))
	 {
		 
	 }
	 elseif(isset($_SESSION["admin"]))
	 {
		 
	 }
	 else
	 {
		 header("Location:login.php");
	 }
?>

<!DOCTYPE html>
<html lang="en">
     <head>
		 <meta charset="utf-8">
		 <meta name="viewport" content="width=device-width, initial-scale=1">
		 <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
		 <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
		 <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
		 <style>
			 body 
			 {
				 font-family: "Lato", sans-serif;
				 margin:0;
			 }
			 .header,h1
			 {
				 text-align: center;
				 font-weight: bold;
			 }
			 .tablink 
			 {
				 background-color: lightblue;
				 color: white;
				 float: left;
				 border: none;
				 outline: none;
				 align:center;
				 cursor: pointer;
				 margin:0;
				 padding: 14px 16px;
				 font-size: 15px;
				 width: 25%;
			 }
			 .tablink:hover 
			 {
				 background-color: #000;
			 }
			 #addadda
			 {
				 background-color: black;
			 }
			 .form-group
			 {
				 width:50em;
			 }
			 .name
			 {
				 font-size: 25px;
			 }
			 .row
			 {
				 margin-right: 1.0em;
				 text-decoration: underline;
				 font-size: 20px;
				 font-family: "Lato", sans-serif;
				 text-align: right;
			 }
			 .footer
			 {
				 position: fixed;
				 bottom: 0;
				 text-align: left;
				 width: 100%;
				 color: white;
				 background-color: black;
			 }
		 </style>
	 </head>
	 
	 <body>
		 <div class="header">
			 <h1>HIRE DAILY WAGE LABOUR</h1>
		 </div>
		 <div class="container-fluid">
			 <form action="index.php">
			 <button class="tablink" >Home</button>
			 </form>
			 <form action="profile.php">
			 <button class="tablink">Profile</button>
			 </form>
			 <form action="search.php">
			 <button class="tablink" >Search</button>
			 </form>
			 <form action="addadda.php">
			 <button class="tablink" id="addadda">Add Adda</button>
			 </form>
		 </div><br>
		 <div class="row" >
				 <a href="logout.php" class="login">Logout</a>
		 </div><br>
		 <div align="center">
		 <div class="form-group" align="left">
		 <form method="POST" action="addaddaphp.php"><br>
			 <label class="name">Add Adda</label><br>
			 <label>ADDA Name:</label>
			 <input type="text" class="form-control" name="aname" required><br>
			 <label>City:</label>
			 <input type="text" class="form-control" name="acity" required><br>
			 <label>ADDA Address:</label>
			 <input type="text" class="form-control" name="aaddress" required><br>
			 <label>ADDA Timings:</label>
			 <input type="text" class="form-control" name="atime" placeholder="EX: 9AM-7PM"><br>
			 <label>ADDA Age:</label>
			 <input type="number" class="form-control" name="aage"><br>
			 <label>What Works They Do:</label>
			 <input type="text" class="form-control" name="aworks" required><br>
			 <label>Other Information If Any:</label>
			 <input type="text" class="form-control" name="aotherinfo"><br>
			 <input type="submit" name="submit" value="Submit">
			 <label>  </label>
			 <input type="reset" name="reset" value="Clear">
		 </form>
		 </div>
		 </div>
		 <br><br><br>
		 <div class="footer">Copyright© 2018, Chidhagni</div>
	 </body>
</html>
<?php $conn->close(); ?>