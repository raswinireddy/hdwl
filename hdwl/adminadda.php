<?php
	 $conn = mysqli_connect("127.0.0.1","root","","dbhdwl");
	 if (!$conn) 
	 {
		 die("Connection failed: " . mysqli_connect_error(). mysqli_connect_errno() . PHP_EOL);
	 } 
?>
<!DOCTYPE html>
<html lang="en">
     <head>
		 <meta charset="utf-8">
		 <meta name="viewport" content="width=device-width, initial-scale=1">
		 <script src="http://code.jquery.com/jquery-1.12.4.min.js"/>	
		 <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.2/css/bootstrap.min.css" integrity="sha384-y3tfxAZXuh4HwSYylfB+J125MxIs6mR5FOHamPBG064zB+AFeWH94NdvaCBm8qnd" crossorigin="anonymous">
		 <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
		 <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
		 <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
		 <link href="http://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css">
         <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.2/css/bootstrap.min.css" integrity="sha384-y3tfxAZXuh4HwSYylfB+J125MxIs6mR5FOHamPBG064zB+AFeWH94NdvaCBm8qnd" crossorigin="anonymous">
		 <link href="pagination/jquery.paginate.css" rel="stylesheet" type="text/css">
		 <script src="pagination/jquery.paginate.js"></script>
		 <style>
			 body 
			 {
				 font-family: "Lato", sans-serif;
				 margin:0;
			 }
			 .header,h1
			 {
				 text-align: center;
				 font-weight: bold;
			 }
			 .tablink 
			 {
				 background-color: lightblue;
				 color: white;
				 float: left;
				 border: none;
				 outline: none;
				 align:center;
				 cursor: pointer;
				 margin:0;
				 padding: 14px 16px;
				 font-size: 15px;
				 width: 20%;
			 }
			 .tablink2
			 {
				 
				 margin-left:1.0em;
				 width:23%;
			 }
			 .tablink:hover 
			 {
				 background-color: #000;
			 }
			 #adminadda
			 {
				 background-color: black;
			 }
			 #label
			 {
				 margin-left:2.0em;
			 }
			 .footer
			 {
				 position: fixed;
				 bottom: 0;
				 text-align: left;
				 width: 100%;
				 color: white;
				 background-color: black;
			 }
		 </style>
	 </head>
	 
	 <body>
		 <div class="header">
			 <h1>HIRE DAILY WAGE LABOUR</h1>
		 </div>
		 <div class="container-fluid">
			 <form action="adminhome.php">
			 <button class="tablink" >Home</button>
			 </form>
			 <form action="adminemp.php">
			 <button class="tablink" >Companies</button>
			 </form>
			 <form action="adminmm.php">
			 <button class="tablink" >MiddleMen</button>
			 </form>
			 <form action="adminadda.php">
			 <button class="tablink" id="adminadda" >Labour Addas</button>
			 </form>
			 <form action="adminlabour.php">
			 <button class="tablink" >Labour</button>
			 </form>
		 </div><br>
		 <div class="row">
			 <div class="col-sm-6">
				 <label id="label">Select city: <label>
				 <select name="city" id="city">
				 <?php
					 $sql="select distinct(City) from adda where Status='active'";
					 $selectbox = $conn->query($sql);
					 while($row=mysqli_fetch_assoc($selectbox))
					 { ?>
					 <option><?php echo $row['City']; ?></option>
                 <?php } ?>
				 </select>
				 <input type="button" value="Search" id="search" onclick="search()">
			 </div>
			 <div class="col-sm-3">
				 <a href="addadda.php" >Add Adda</a>
			 </div>
			 <div class="col-sm-3">
				 <a href="logout.php" class="logout">Logout</a>
			 </div>
		 </div><br>
		 
		 <div id="resultdiv">
<?php $sql1="select * from adda where Status='active'";
	 $result = $conn->query($sql1);
?>
		 <table align="center" cellpadding="10" border="1" width="1000" id="table1">
			 <thead>
			 <tr>
			 <?php $finfo = $result->fetch_fields();
			 foreach($finfo as $val) 
			 {
				 if(($val->name!=="Status")&&($val->name!=="Timings")&&($val->name!=="Age")&&($val->name!=="Address")&&($val->name!=="Owner")&&($val->name!=="Ownertype"))
				 { ?>
					 <th align='center'><?php echo $val->name; ?></th>
				 <?php }
			 } ?>
			 <th></th>
			 </tr></thead>
			 <tbody>
<?php
	 while($row = $result->fetch_assoc())
	 { ?>
		 <tr id="row<?php echo $row['Name']; ?>">
			 <td id="fname<?php echo $row['Name']; ?>"><?php echo $row['Name'] ?></td>
			 <td><?php echo $row['City'] ?></td>
			 <td><?php echo $row['Timings'] ?></td>
			 <td><?php echo $row['Works'] ?></td>
			 <td><input type='button' value='View' id='view' onclick='viewrow("<?php echo $row['Name'] ?>")'>
				 <input type='button' value='Edit' id='edit' onclick='editrow("<?php echo $row['Name'] ?>")'>
				 <input type='button' value='Delete' id='delete' onclick='deleterow("<?php echo $row['Name'] ?>")'>
			 </td>
		 </tr>
	 <?php } ?>
		 </tbody>
		 </table>
		 </div><br>
		 <div id="viewdiv"></div>
		 <br><br><br>
		 <div class="footer">Copyright© 2018, Chidhagni</div>
		 <script>
             function search()
			 {
			 	 var scity=document.getElementById("city").value;
			 	 $.ajax({
			 	 	 url:'adminaddasearch.php',
			 	 	 data: {'city':scity},
			 	 	 success: function(data)
			 	 	 {
			 	 	 	 $('#resultdiv').html(data);
						 $("#table2").paginate(
						 {
							 "elemsPerPage": 5,
							 "maxButtons": 6
						 });
			 	 	 }
			 	 });
			 }
			 function viewrow(sname)
			 {
				 $.ajax({
					 url: 'adminaddaview.php',
					 data: {'name':sname},
					 success: function(data)
					 {
						 $('#viewdiv').html(data);
					 }
				 });
			 }
			 function editrow(sname)
			 {
				 window.location.href="adminaddaedit.php?name="+sname;
			 }
			 function deleterow(sname)
			 {
				 window.location.href="adminaddadelete.php?name="+sname;
			 }
			 $("#table1").paginate(
			 {
				 "elemsPerPage": 5,
				 "maxButtons": 6
			 });
		 </script>
		 </body>
</html>
<?php $conn->close(); ?>