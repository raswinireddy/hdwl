<?php
	 $conn = mysqli_connect("127.0.0.1","root","","dbhdwl");
	 if (!$conn)
	 {
		 die("Connection failed: " . mysqli_connect_error(). mysqli_connect_errno() . PHP_EOL);
	 }
?>

<!DOCTYPE html>
<html lang="en">
     <head>
		 <meta charset="utf-8">
		 <meta name="viewport" content="width=device-width, initial-scale=1">
		 <script src="http://code.jquery.com/jquery-1.12.4.min.js"/>
		 <link href="jquery.paginate.css" rel="stylesheet" type="text/css">
		 <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.2/css/bootstrap.min.css" integrity="sha384-y3tfxAZXuh4HwSYylfB+J125MxIs6mR5FOHamPBG064zB+AFeWH94NdvaCBm8qnd" crossorigin="anonymous">
		 <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
		 <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
		 <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
		 <link href="http://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css">
         <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.2/css/bootstrap.min.css" integrity="sha384-y3tfxAZXuh4HwSYylfB+J125MxIs6mR5FOHamPBG064zB+AFeWH94NdvaCBm8qnd" crossorigin="anonymous">
		 <link href="pagination/jquery.paginate.css" rel="stylesheet" type="text/css">
		 <script src="pagination/jquery.paginate.js"></script>
		 <style>
			 body
			 {
				 font-family: "Lato", sans-serif;
				 margin:0;
			 }
			 .header,h1
			 {
				 text-align: center;
				 font-weight: bold;
			 }
			 .tablink
			 {
				 background-color: lightblue;
				 color: white;
				 float: left;
				 border: none;
				 outline: none;
				 align:center;
				 cursor: pointer;
				 margin:0;
				 padding: 14px 16px;
				 font-size: 15px;
				 width: 20%;
			 }
			 .tablink2
			 {

				 margin-left:1.0em;
				 width:23%;
			 }
			 .tablink:hover
			 {
				 background-color: #000;
			 }
			 #adminlabour
			 {
				 background-color: black;
			 }
			 #label
			 {
				 margin-left:2.0em;
			 }
			 .form-group
			 {
				 width:50em;
			 }
			 .name
			 {
				 font-size: 25px;
			 }
			 .row
			 {
				 margin-right: 1.0em;
				 text-decoration: underline;
				 font-size: 20px;
				 font-family: "Lato", sans-serif;
				 text-align: right;
			 }
			 .footer
			 {
				 position: fixed;
				 bottom: 0;
				 text-align: left;
				 width: 100%;
				 color: white;
				 background-color: black;
			 }
		 </style>
	 </head>

	 <body>
		 <div class="header">
			 <h1>HIRE DAILY WAGE LABOUR</h1>
		 </div>
		 <div class="container-fluid">
			 <form action="adminhome.php">
			 <button class="tablink" >Home</button>
			 </form>
			 <form action="adminemp.php">
			 <button class="tablink" >Companies</button>
			 </form>
			 <form action="adminmm.php">
			 <button class="tablink" >MiddleMen</button>
			 </form>
			 <form action="adminadda.php">
			 <button class="tablink" >Labour Addas</button>
			 </form>
			 <form action="adminlabour.php">
			 <button class="tablink" id="adminlabour" >Labour</button>
			 </form>
		 </div><br>
		 <div class="row" >
				 <a href="logout.php" class="login">Logout</a>
		 </div><br>
		 <div align="center">
		 <div class="form-group" align="left">
		 <form method="POST" action="adminaddlabourphp.php"><br>
			 <label class="name">Add Labour</label><br>
			 <label>First Name:</label>
			 <input type="text" class="form-control" name="fname" required><br>
			 <label>Last Name:</label>
			 <input type="text" class="form-control" name="lname" required><br>
			 <label>Gender:</label>
			 <input type="text" class="form-control" name="gender" required><br>
			 <label>Aadhar Number:</label>
			 <input type="text" class="form-control" name="aadhar"><br>
			 <label>Mobile number:</label>
			 <input type="text" class="form-control" name="mobile"><br>
			 <label>City:</label>
			 <input type="text" class="form-control" name="city" required><br>
			 <label>Address:</label>
			 <input type="text" class="form-control" name="address"><br>
			 <input type="submit" name="submit" value="Submit">
			 <label>  </label>
			 <input type="reset" name="reset" value="Clear">
		 </form>
		 </div>
		 </div>
		 <br><br><br>
		 <div class="footer">Copyright© 2018, Chidhagni</div>
	 </body>
</html>
<?php $conn->close(); ?>