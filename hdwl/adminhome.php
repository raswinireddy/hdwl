<?php
	 $conn = mysqli_connect("127.0.0.1","root","","dbhdwl");
	 if (!$conn) 
	 {
		 die("Connection failed: " . mysqli_connect_error(). mysqli_connect_errno() . PHP_EOL);
	 } 
?>
<!DOCTYPE html>
<html lang="en">
     <head>
		 <meta charset="utf-8">
		 <meta name="viewport" content="width=device-width, initial-scale=1">
		 <script src="http://code.jquery.com/jquery-1.12.4.min.js"/>	
		 <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.2/css/bootstrap.min.css" integrity="sha384-y3tfxAZXuh4HwSYylfB+J125MxIs6mR5FOHamPBG064zB+AFeWH94NdvaCBm8qnd" crossorigin="anonymous">
		 <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
		 <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
		 <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
		 <link href="http://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css">
		 <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.2/css/bootstrap.min.css" integrity="sha384-y3tfxAZXuh4HwSYylfB+J125MxIs6mR5FOHamPBG064zB+AFeWH94NdvaCBm8qnd" crossorigin="anonymous">
		 <link href="pagination/jquery.paginate.css" rel="stylesheet" type="text/css">
		 <style>
			 body 
			 {
				 font-family: "Lato", sans-serif;
				 margin:0;
			 }
			 .header,h1
			 {
				 text-align: center;
				 font-weight: bold;
			 }
			 .tablink 
			 {
				 background-color: lightblue;
				 color: white;
				 float: left;
				 border: none;
				 outline: none;
				 align:center;
				 cursor: pointer;
				 margin:0;
				 padding: 14px 16px;
				 font-size: 15px;
				 width: 20%;
			 }
			 .tablink2
			 {
				 
				 margin-left:1.0em;
				 width:23%;
			 }
			 .tablink:hover 
			 {
				 background-color: #000;
			 }
			 #adminhome
			 {
				 background-color: black;
			 }
			 .footer
			 {
				 position: fixed;
				 bottom: 0;
				 text-align: left;
				 width: 100%;
				 color: white;
				 background-color: black;
			 }
		 </style>
	 </head>
	 
	 <body>
		 <div class="header">
			 <h1>HIRE DAILY WAGE LABOUR</h1>
		 </div>
		 <div class="container-fluid">
			 <form action="adminhome.php">
			 <button class="tablink" id="adminhome" >Home</button>
			 </form>
			 <form action="adminemp.php">
			 <button class="tablink" >Companies</button>
			 </form>
			 <form action="adminmm.php">
			 <button class="tablink" >MiddleMen</button>
			 </form>
			 <form action="adminadda.php">
			 <button class="tablink" >Labour Addas</button>
			 </form>
			 <form action="adminlabour.php">
			 <button class="tablink" >Labour</button>
			 </form>
		 </div><br>
		 <div class="container-fluid">
				 <button class="tablink2" id="emp">View Companies</button>
				 	 
				 <button class="tablink2" id="mm">View MiddleMen</button>
				 <button class="tablink2" id="adda">View Labour Addas</button>
			
				 <button class="tablink2" id="labour">View Labour</button>
		 </div>
		 <br><br>
		 <script>
		 $(document).ready(function()
		 {
			 $("#emp").click(function()
			 {
				 $.ajax(
				 {
					 url:'getcompanies.php',
					 success: function(result)
					 {
						 $("#resultdiv").html(result);
						 $("#table1").paginate(
						 {
							 "elemsPerPage": 5,
							 "maxButtons": 6
						 });
					 }
				 });
			 });
			 $("#mm").click(function()
			 {
				 $.ajax(
				 {
					 url:'getmiddleman.php',
					 success: function(result)
					 {
						 $('#resultdiv').html(result);
						 $("#table2").paginate(
						 {
							 "elemsPerPage": 5,
							 "maxButtons": 6
						 });
					 }
				 });
			 });
			 $("#adda").click(function()
			 {
				 $.ajax(
				 {
					 url:'getadda.php',
					 success: function(result)
					 {
						 $('#resultdiv').html(result);
						 $("#table3").paginate(
						 {
							 "elemsPerPage": 5,
							 "maxButtons": 6
						 });
					 }
				 });
			 });
			 $("#labour").click(function()
			 {
				 $.ajax(
				 {
					 url:'getlabour.php',
					 success: function(result)
					 {
						 $('#resultdiv').html(result);
						 $("#table4").paginate(
						 {
							 "elemsPerPage": 5,
							 "maxButtons": 6
						 });
					 }
				 });
			 });
		 });
		 </script>
		 <div id="resultdiv"></div>
		 <br><br><br>
		 <div class="footer">Copyright© 2018, Chidhagni</div>
		 <script src="pagination/jquery.paginate.js"></script>
	 </body>
</html>
<?php $conn->close(); ?>