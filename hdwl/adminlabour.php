<?php
	 $conn = mysqli_connect("127.0.0.1","root","","dbhdwl");
	 if (!$conn)
	 {
		 die("Connection failed: " . mysqli_connect_error(). mysqli_connect_errno() . PHP_EOL);
	 }
?>

<!DOCTYPE html>
<html lang="en">
     <head>
		 <meta charset="utf-8">
		 <meta name="viewport" content="width=device-width, initial-scale=1">
		 <script src="http://code.jquery.com/jquery-1.12.4.min.js"/>
		 <link href="jquery.paginate.css" rel="stylesheet" type="text/css">
		 <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.2/css/bootstrap.min.css" integrity="sha384-y3tfxAZXuh4HwSYylfB+J125MxIs6mR5FOHamPBG064zB+AFeWH94NdvaCBm8qnd" crossorigin="anonymous">
		 <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
		 <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
		 <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
		 <link href="http://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css">
         <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.2/css/bootstrap.min.css" integrity="sha384-y3tfxAZXuh4HwSYylfB+J125MxIs6mR5FOHamPBG064zB+AFeWH94NdvaCBm8qnd" crossorigin="anonymous">
		 <link href="pagination/jquery.paginate.css" rel="stylesheet" type="text/css">
		 <script src="pagination/jquery.paginate.js"></script>
		 <style>
			 body
			 {
				 font-family: "Lato", sans-serif;
				 margin:0;
			 }
			 .header,h1
			 {
				 text-align: center;
				 font-weight: bold;
			 }
			 .tablink
			 {
				 background-color: lightblue;
				 color: white;
				 float: left;
				 border: none;
				 outline: none;
				 align:center;
				 cursor: pointer;
				 margin:0;
				 padding: 14px 16px;
				 font-size: 15px;
				 width: 20%;
			 }
			 .tablink2
			 {

				 margin-left:1.0em;
				 width:23%;
			 }
			 .tablink:hover
			 {
				 background-color: #000;
			 }
			 #adminlabour
			 {
				 background-color: black;
			 }
			 #label
			 {
				 margin-left:2.0em;
			 }
			 .footer
			 {
				 position: fixed;
				 bottom: 0;
				 text-align: left;
				 width: 100%;
				 color: white;
				 background-color: black;
			 }
		 </style>
	 </head>

	 <body>
		 <div class="header">
			 <h1>HIRE DAILY WAGE LABOUR</h1>
		 </div>
		 <div class="container-fluid">
			 <form action="adminhome.php">
			 <button class="tablink" >Home</button>
			 </form>
			 <form action="adminemp.php">
			 <button class="tablink" >Companies</button>
			 </form>
			 <form action="adminmm.php">
			 <button class="tablink" >MiddleMen</button>
			 </form>
			 <form action="adminadda.php">
			 <button class="tablink" >Labour Addas</button>
			 </form>
			 <form action="adminlabour.php">
			 <button class="tablink" id="adminlabour" >Labour</button>
			 </form>
		 </div><br>
		 <div class="row">
			 <div class="col-sm-3">
				 <label id="label">Select city: <label>
				 <select name="city" id="city">
				 <?php
					 $sql="select distinct(City) from labour where Status='active'";
					 $selectbox = $conn->query($sql);
					 while($row=mysqli_fetch_assoc($selectbox))
					 { ?>
					 <option><?php echo $row['City']; ?></option>
                 <?php } ?>
				 </select>
				 <input type="button" value="Search" id="search" onclick="search()">
			 </div>
			 <div class="col-sm-2">
				 <a href="template.xlsx" download="Template">Download Template</a>
			 </div>
			 <div class="col-sm-4">
				 <form method="POST" enctype="multipart/form-data">
					 <input type="file" name="file" />
					 <input type="submit" name="upload" value="Upload">
				 </form>
			 </div>
			 <div class="col-sm-2">
				 <a href="adminaddlabour.php" >Add Labour</a>
			 </div>
			 <div class="col-sm-1">
				 <a href="logout.php" class="logout">Logout</a>
			 </div>
		 </div><br>
		 
<?php
	 $status='active';
	 if(isset($_POST["upload"]))
	 {
		 if($_FILES['file']['name'])
		 {
			 $filename=explode('.',$_FILES['file']['name']);
			 if($filename[1]=='csv')
			 {
				 $handle=fopen($_FILES['file']['tmp_name'], 'r');
				 while($data=fgetcsv($handle))
				 {
					 $item1=mysqli_real_escape_string($conn, $data[0]);
					 $item2=mysqli_real_escape_string($conn, $data[1]);
					 $item3=mysqli_real_escape_string($conn, $data[2]);
					 $item4=mysqli_real_escape_string($conn, $data[3]);
					 $item5=mysqli_real_escape_string($conn, $data[4]);
					 $item6=mysqli_real_escape_string($conn, $data[5]);
					 $item7=mysqli_real_escape_string($conn, $data[6]);
					 $sql="insert into labour values('$item1','$item2','$item3','$item4','$item5','$item6','$item7','$status')";
					 if($conn->query($sql))
					 {
						 
					 }
					 else
					 {
						 echo "Error: ".mysqli_error($conn);
					 }
				 }
				 fclose($handle);
			 }
		 }
	 }
?>

		 <div id="resultdiv">
<?php $sql1="select * from labour where Status='active'";
	 $result = $conn->query($sql1);
?>
		 <table align="center" cellpadding="10" border="1" width="1000" id="table">
			 <thead>
			 <tr>
			 <?php $finfo = $result->fetch_fields();
			 foreach($finfo as $val)
			 {
				 if(($val->name!=="Status")&&($val->name!=="MobileNumber")&&($val->name!=="City")&&($val->name!=="Address"))
				 { ?>
					 <th align='center'><?php echo $val->name; ?></th>
				 <?php }
			 } ?>
			 <th></th>
			 </tr></thead>
			 <tbody>
<?php
	 while($row = $result->fetch_assoc())
	 { ?>
		 <tr id="row<?php echo $row['AadharNumber']; ?>">
			 <td><?php echo $row['FirstName'] ?></td>
			 <td><?php echo $row['LastName'] ?></td>
			 <td><?php echo $row['Gender'] ?></td>
			 <td id="fname<?php echo $row['AadharNumber']; ?>"><?php echo $row['AadharNumber'] ?></td>
			 <td><input type='button' value='View' id='view' onclick='viewrow("<?php echo $row['AadharNumber'] ?>")'>
				 <input type='button' value='Edit' id='edit' onclick='editrow("<?php echo $row['AadharNumber'] ?>")'>
				 <input type='button' value='Delete' id='delete' onclick='deleterow("<?php echo $row['AadharNumber'] ?>")'>
			 </td>
		 </tr>
	 <?php } ?>
		 </tbody>
		 </table>
		 </div><br>
		 <div id="viewdiv"></div>
		 <br><br><br>
		 <div class="footer">Copyright© 2018, Chidhagni</div>
		 <script>
			 /*function upload()
			 {
			 	 var sfile=document.getElementById("file").value;
				 console.log(sfile);
			 	 $.ajax({
			 	 	 url:'uploadexcel1.php',
			 	 	 data: {'file':sfile},
			 	 	 success: function(data)
			 	 	 {
			 	 	 	 $('#resultdiv').html(data);
			 	 	 }
			 	 });
			 }*/
			 function search()
			 {
			 	 var scity=document.getElementById("city").value;
			 	 $.ajax({
			 	 	 url:'adminlaboursearch.php',
			 	 	 data: {'city':scity},
			 	 	 success: function(data)
			 	 	 {
			 	 	 	 $('#resultdiv').html(data);
						 $("#table1").paginate(
						 {
							 "elemsPerPage": 5,
							 "maxButtons": 6
						 });
			 	 	 }
			 	 });
			 }
			 function viewrow(saadhar)
			 {
				 $.ajax({
					 url: 'adminlabourview.php',
					 data: {'aadhar':saadhar},
					 success: function(data)
					 {
						 $('#viewdiv').html(data);
					 }
				 });
			 }
			 function editrow(saadhar)
			 {
				 window.location.href="adminlabouredit.php?aadhar="+saadhar;
			 }
			 function deleterow(saadhar)
			 {
				 window.location.href="adminlabourdelete.php?aadhar="+saadhar;
			 }
			 $("#table").paginate(
			 {
				 "elemsPerPage": 5,
				 "maxButtons": 6
			 });
		 </script>
		 </body>
</html>


<?php $conn->close(); ?>