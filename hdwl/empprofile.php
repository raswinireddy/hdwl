<!DOCTYPE html>
<html lang="en">
     <head>
		 <meta charset="utf-8">
		 <meta name="viewport" content="width=device-width, initial-scale=1">
		 <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
		 <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
		 <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
		 <style>
			 body
			 {
				 font-family: "Lato", sans-serif;
				 margin:0;
			 }
			 .header,h1
			 {
				 text-align: center;
				 font-weight: bold;
			 }
			 .tablink 
			 {
				 background-color: lightblue;
				 color: white;
				 float: left;
				 border: none;
				 outline: none;
				 align:center;
				 cursor: pointer;
				 margin:0;
				 padding: 14px 16px;
				 font-size: 15px;
				 width: 25%;
			 }
			 .tablink:hover 
			 {
				 background-color: #000;
			 }
			 #profile
			 {
				 background-color: black;
			 }
			 .row
			 {
				 margin-right: 1.0em;
				 text-decoration: underline;
				 font-size: 20px;
				 font-family: "Lato", sans-serif;
				 text-align: right;
			 }
			 .footer
			 {
				 position: fixed;
				 bottom: 0;
				 width: 100%;
				 text-align: left;
				 color: white;
				 background-color: black;
			 }
		 </style>
	 </head>
	 
	 <body>
		 <div class="header">
			 <h1>HIRE DAILY WAGE LABOUR</h1>
		 </div>
		 <div class="container-fluid">
			 <form action="index.php">
			 <button class="tablink" >Home</button>
			 </form>
			 <form action="profile.php">
			 <button class="tablink" id="profile">Profile</button>
			 </form>
			 <form action="search.php">
			 <button class="tablink" >Search</button>
			 </form>
			 <form action="addadda.php">
			 <button class="tablink" >Add Adda</button>
			 </form>
		 </div><br>
		 
		 <div class="row" >
				 <a href="logout.php" class="login">Logout</a>
		 </div><br>
<?php
	 session_start();
	 $conn = mysqli_connect("127.0.0.1","root","","dbhdwl");
	 if (!$conn) 
	 {
		 die("Connection failed: " . mysqli_connect_error(). mysqli_connect_errno() . PHP_EOL);
	 }
	 $email=$_SESSION["email"];
	 $sql1="select * from emp where Email='$email' and Status='active'";
	 $result = $conn->query($sql1);
?>
	 <div class="container-fluid">
	 <div class="col-sm-6">
	 <form method='post' action='empeditui.php'>
	 <table align='center' cellpadding="10" width="500">
	 <?php
		 if($result->num_rows > 0)
		 {
			 $finfo = $result->fetch_fields();
			 while($row = $result->fetch_assoc())
			 {
				 foreach($finfo as $val)
				 { ?>
					 <tr>
					 <?php if($val->name!=="Status")
					 { ?>
						 <th align='center'><?php echo $val->name; ?></th>
						 <td><?php echo $row[$val->name]; ?></td>
					 <?php } ?>
					 </tr>
				 <?php } ?>
				 <tr> 
				 <td align='center' colspan='2' ><input type='submit' value='Edit Profile'></td>
				 </tr>
			 <?php }
		 } ?>
	 </table>
	 </form>
	 </div>
	 <div class="col-sm-6">
		 <label align="center">Your Addas</label>
	 <table align='center' cellpadding="10" width="500">
	 <?php
	 	 $sql="select * from adda where Owner='$email' and Status='active' and Ownertype='emp'";
		 $result = $conn->query($sql);
		 $finfo = $result->fetch_fields();
		 foreach($finfo as $val)
		 {
			 if(($val->name!=="Status")&&($val->name!=="Owner")&&($val->name!=="Ownertype"))
			 { ?>
				 <th align='center'><?php echo $val->name; ?></th>				 
			 <?php }
		 } ?>
		 <tr>
		 <?php if($result->num_rows > 0)
		 {
			 $finfo = $result->fetch_fields();
			 while($row = $result->fetch_assoc())
			 {
				 foreach($finfo as $val)
				 { ?>
					 <?php if(($val->name!=="Status")&&($val->name!=="Owner")&&($val->name!=="Ownertype"))
					 { ?>
						 <td><?php echo $row[$val->name]; ?></td>
					 <?php } ?>
				 <?php } ?>
				 <td align='center' colspan='2' ><input type="button" value='Edit Adda' onclick='edit("<?php echo $row['Name']; ?>")'>
				 </td>
				 </tr>
			 <?php }
		 } ?>
		 
	 </table>
	 </div>
	 <br><br><br>
	 <div class="footer">Copyright© 2018, Chidhagni</div>
	 <script>
		 function edit(sname)
		 {
			 console.log(sname);
			 window.location.href="empaddaeditui.php?adda="+sname;
		 }
	 </script>
	 </body>	 
</html>
<?php $conn->close(); ?>

