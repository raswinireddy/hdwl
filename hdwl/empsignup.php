<!DOCTYPE html>
<html lang="en">
     <head>
		 <meta charset="utf-8">
		 <meta name="viewport" content="width=device-width, initial-scale=1">
		 <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
		 <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
		 <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
		 <style>
			 body 
			 {
				 font-family: "Lato", sans-serif;
				 margin:0;
			 }
			 .header,h1
			 {
				 text-align: center;
				 font-weight: bold;
			 }
			 .tablink 
			 {
				 background-color: lightblue;
				 color: white;
				 float: left;
				 border: none;
				 outline: none;
				 align:center;
				 cursor: pointer;
				 margin:0;
				 padding: 14px 16px;
				 font-size: 15px;
				 width: 25%;
			 }
			 .tablink:hover 
			 {
				 background-color: #000;
			 }
			 #home
			 {
				 background-color: black;
			 }
			 .form-group
			 {
				 width:50em;
			 }
			 .name
			 {
				 font-size: 25px;
			 }
			 .footer
			 {
				 position: fixed;
				 bottom: 0;
				 text-align: left;
				 width: 100%;
				 color: white;
				 background-color: black;
			 }
		 </style>
	 </head>
	 
	 <body>
		 <div class="header">
			 <h1>HIRE DAILY WAGE LABOUR</h1>
		 </div>
		 <div class="container-fluid">
			 <form action="index.php">
			 <button class="tablink" id="home" >Home</button>
			 </form>
			 <form action="login.php">
			 <button class="tablink">Login</button>
			 </form>
			 <form action="search.php">
			 <button class="tablink" >Search</button>
			 </form>
			 <form action="addadda.php">
			 <button class="tablink" >Add Adda</button>
			 </form>
		 </div><br>
		 <div align="center">
		 <div class="form-group" align="left">
		 <form method="POST" action="empsignupphp.php"><br>
			 <label class="name">Company/Employer Sign Up</label><br>
			 <label>Company Name:</label>
			 <input type="text" class="form-control" name="companyname" required><br>
			 <label>First Name:</label>
			 <input type="text" class="form-control" name="fname" required><br>
			 <label>Last Name:</label>
			 <input type="text" class="form-control" name="lname" required><br>
			 <label>Aadhar Number:</label>
			 <input type="text" class="form-control" name="aadhar" required><br>
			 <label>Mobile Number:</label>
			 <input type="text" class="form-control" name="mobile" required><br>
			 <label>City:</label>
			 <input type="text" class="form-control" name="city" required><br>
			 <label>Address:</label>
			 <input type="text" class="form-control" name="address" required><br>
			 <label>Email:</label>
			 <input type="email" class="form-control" name="email" required><br>
			 <label>Password:</label>
			 <input type="password" class="form-control" name="pwd" required><br>
			 <input type="submit" name="submit" value="Sign Up">
			 <label>  </label>
			 <input type="reset" name="reset" value="Clear">
		 </form>
		 </div>
		 </div>
		 <br><br><br>
		 <div class="footer">Copyright© 2018, Chidhagni</div>
	 </body>
</html>