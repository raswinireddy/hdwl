<html>
	 <head>
		 <script src="pagination/jquery.paginate.js"></script>
		 <script src="https://code.jquery.com/jquery-1.12.0.min.js"></script>
		 <script src="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.concat.min.js"></script>
	 </head>
	 <body>
		 <div id="wrapper1">
<?php
	 $conn = mysqli_connect("127.0.0.1","root","","dbhdwl");
	 if (!$conn) 
	 {
		 die("Connection failed: " . mysqli_connect_error(). mysqli_connect_errno() . PHP_EOL);
	 }
	 $sql1="select Name,City,Timings,Works from adda where Status='active'";
	 $result = $conn->query($sql1);
?>
		 <table align="center" cellpadding="10" border="1" width="1000" id="table3">
			 <thead>
			 <tr>
			 <?php $finfo = $result->fetch_fields();
			 foreach($finfo as $val) 
			 {
				 if($val->name!=="Status")
				 { ?>
					 <th align='center'><?php echo $val->name; ?></th>
				 <?php }
			 } ?>
			 <th></th>
			 </tr></thead>
			 <tbody>
<?php
	 while($row = $result->fetch_assoc())
	 { ?>
		 <tr id="row<?php echo $row['Name']; ?>">
			 <td id="fname<?php echo $row['Name']; ?>"><?php echo $row['Name'] ?></td>
			 <td><?php echo $row['City'] ?></td>
			 <td><?php echo $row['Timings'] ?></td>
			 <td><?php echo $row['Works'] ?></td>
			 <td><input type='button' value='View' id='view' onclick='viewrow("<?php echo $row['Name'] ?>")'>
				 <input type='button' value='Edit' id='edit' onclick='editrow("<?php echo $row['Name'] ?>")'>
				 <input type='button' value='Delete' id='delete' onclick='deleterow("<?php echo $row['Name'] ?>")'>
			 </td>
		 </tr>
	 <?php } ?>
		 </tbody>
		 </table>
		 </div><br>
		 <div id="wrapper"></div>
		 <script>
			 function viewrow(sname)
			 {
				 //console.log(semail);
				 //var email=semail;
				 $.ajax({
					 url: 'adminviewadda.php',
					 data: {'name':sname},
					 success: function(data)
					 {
						 $('#wrapper').html(data);
					 }
				 });
			 }
			 function editrow(sname)
			 {
				 //var mail= semail;
				 window.location.href="admineditadda.php?name="+sname;
			 }
			 function deleterow(sname)
			 {
				 //var mail= semail;
				 window.location.href="admindeleteadda.php?name="+sname;
			 }
		 </script>
	 </body>
</html>
<?php $conn->close(); ?>

