<html>
	 <head>
		 <script src="https://code.jquery.com/jquery-1.12.0.min.js"></script>
		 <script src="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.concat.min.js"></script>
		 <script src="pagination/jquery.paginate.js"></script>
	 </head>
	 <body>
		 <div id="wrapper1">
<?php
	 $conn = mysqli_connect("127.0.0.1","root","","dbhdwl");
	 if (!$conn) 
	 {
		 die("Connection failed: " . mysqli_connect_error(). mysqli_connect_errno() . PHP_EOL);
	 }
	 $sql1="select * from labour where Status='active'";
	 $result = $conn->query($sql1);
?>
		 <table align="center" cellpadding="10" border="1" width="1000" id="table4">
			 <thead>
			 <tr>
			 <?php $finfo = $result->fetch_fields();
			 foreach($finfo as $val) 
			 {
				 if(($val->name!=="Status")&&($val->name!=="MobileNumber")&&($val->name!=="City")&&($val->name!=="Address"))
				 { ?>
					 <th align='center'><?php echo $val->name; ?></th>
				 <?php }
			 } ?>
			 <th></th>
			 </tr></thead>
			 <tbody>
<?php
	 while($row = $result->fetch_assoc())
	 { ?>
		 <tr id="row<?php echo $row['AadharNumber']; ?>">
			 <td><?php echo $row['FirstName'] ?></td>
			 <td><?php echo $row['LastName'] ?></td>
			 <td><?php echo $row['Gender'] ?></td>
			 <td id="fname<?php echo $row['AadharNumber']; ?>"><?php echo $row['AadharNumber'] ?></td>
			 <td><input type='button' value='View' id='view' onclick='viewrow("<?php echo $row['AadharNumber'] ?>")'>
				 <input type='button' value='Edit' id='edit' onclick='editrow("<?php echo $row['AadharNumber'] ?>")'>
				 <input type='button' value='Delete' id='delete' onclick='deleterow("<?php echo $row['AadharNumber'] ?>")'>
			 </td>
		 </tr>
	 <?php } ?>
		 </tbody>
		 </table>
		 </div><br>
		 <div id="wrapper"></div>
		 <script>
			 function viewrow(saadhar)
			 {
				 $.ajax({
					 url: 'adminviewlabour.php',
					 data: {'aadhar':saadhar},
					 success: function(data)
					 {
						 $('#wrapper').html(data);
					 }
				 });
			 }
			 function editrow(saadhar)
			 {
				 window.location.href="admineditlabour.php?aadhar="+saadhar;
			 }
			 function deleterow(saadhar)
			 {
				 window.location.href="admindeletelabour.php?aadhar="+saadhar;
			 }
		 </script>
	 </body>
</html>
<?php $conn->close(); ?>