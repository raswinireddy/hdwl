<html>
	 <head>
		 <script src="pagination/jquery.paginate.js"></script>
		 <script src="https://code.jquery.com/jquery-1.12.0.min.js"></script>
		 <script src="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.concat.min.js"></script>
	 </head>
	 <body>
		 <div id="wrapper1">
<?php
	 $conn = mysqli_connect("127.0.0.1","root","","dbhdwl");
	 if (!$conn)
	 {
		 die("Connection failed: " . mysqli_connect_error(). mysqli_connect_errno() . PHP_EOL);
	 }
	 $sql1="select FirstName,LastName,MobileNumber,Email from mm where Status='active'";
	 $result = $conn->query($sql1);
?>
		 <table align="center" cellpadding="10" border="1" width="1000" id="table2">
			 <thead>
			 <tr>
			 <?php $finfo = $result->fetch_fields();
			 foreach($finfo as $val)
			 {
				 if(($val->name!=="Password")&&($val->name!=="Status"))
				 { ?>
					 <th align='center'><?php echo $val->name; ?></th>
				 <?php }
			 } ?>
			 <th></th>
			 </tr><thead>
			 <tbody>
<?php
	 while($row = $result->fetch_assoc())
	 { ?>
			 <tr id="row<?php echo $row['Email']; ?>">
			 <td><?php echo $row['FirstName'] ?></td>
			 <td><?php echo $row['LastName'] ?></td>
			 <td><?php echo $row['MobileNumber'] ?></td>
			 <td id="fname<?php echo $row['Email']; ?>"><?php echo $row['Email'] ?></td>
			 <td><input type='button' value='View' id='view' onclick='viewrow("<?php echo $row['Email'] ?>")'>
				 <input type='button' value='Edit' id='edit' onclick='editrow("<?php echo $row['Email'] ?>")'>
				 <input type='button' value='Delete' id='delete' onclick='deleterow("<?php echo $row['Email'] ?>")'>
			 </td>
		 </tr>
	 <?php } ?>
		 </tbody>
		 </table>
		 </div><br>
		 <div id="wrapper"></div>
		 <script>
			 function viewrow(semail)
			 {
				 //console.log(semail);
				 //var email=semail;
				 $.ajax({
					 url: 'adminviewmm.php',
					 data: {'mail':semail},
					 success: function(data)
					 {
						 $('#wrapper').html(data);
					 }
				 });
			 }
			 function editrow(semail)
			 {
				 //var mail= semail;
				 window.location.href="admineditmm.php?email="+semail;
			 }
			 function deleterow(semail)
			 {
				 //var mail= semail;
				 window.location.href="admindeletemm.php?email="+semail;
			 }
		 </script>
	 </body>
</html>
<?php $conn->close(); ?>
