<!DOCTYPE html>
<html lang="en">
     <head>
		 <meta charset="utf-8">
		 <meta name="viewport" content="width=device-width, initial-scale=1">
		 <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
		 <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
		 <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
		 <style>
			 body
			 {
				 font-family: "Lato", sans-serif;
				 margin:0;
			 }
			 .header,h1
			 {
				 text-align: center;
				 font-weight: bold;
			 }
			 .tablink 
			 {
				 background-color: lightblue;
				 color: white;
				 float: left;
				 border: none;
				 outline: none;
				 align:center;
				 cursor: pointer;
				 margin:0;
				 padding: 14px 16px;
				 font-size: 15px;
				 width: 25%;
			 }
			 .tablink:hover 
			 {
				 background-color: #000;
			 }
			 #home
			 {
				 background-color: black;
			 }
			 .signuplink
			 {
				 text-decoration: underline;
				 font-size: 15px;
				 font-family: "Lato", sans-serif;
			 }
			 .pre
			 {
				 font-family: "Lato", sans-serif;
			 }
			 .row
			 {
				 margin-right: 1.0em;
				 text-decoration: underline;
				 font-size: 20px;
				 font-family: "Lato", sans-serif;
				 text-align: right;
			 }
			 .footer
			 {
				 position: fixed;
				 bottom: 0;
				 width: 100%;
				 text-align: left;
				 color: white;
				 background-color: black;
			 }
		 </style>
	 </head>
	 
	 <body>
		 <div class="header">
			 <h1>HIRE DAILY WAGE LABOUR</h1>
		 </div>
		 <div class="container-fluid">
			 <form action="index.php">
			 <button class="tablink" id="home" >Home</button>
			 </form>
			 <form action="profile.php">
			 <button class="tablink" >Profile</button>
			 </form>
			 <form action="search.php">
			 <button class="tablink" >Search</button>
			 </form>
			 <form action="addadda.php">
			 <button class="tablink" >Add Adda</button>
			 </form>
		 </div><br>
		 <div class="row" >
				 <a href="logout.php" class="login">Logout</a>
		 </div><br>
		 <div class="container">
			 <div class="col-sm-1"></div>
			 <div class="col-sm-5" align="left">
				 <h4>Companies/Employers<h4>
				 <pre class="pre">
Employers or Companies who provide the employment to the labour 
can register here to know about the middlemen and the labour addas 
and search through different cities.<br>
				 <div align="right"><a href="empsignup.php" class="signuplink">Sign Up</a></div></pre>
			 </div>
			 <div class="col-sm-5">
				 <h4>MiddleMen<h4>
				 <pre class="pre">
Search about Companies or Employers to provide the employment to 
the labour.
Sign up here to proceed.<br>
				 <div align="right"><a href="mmsignup.html" class="signuplink">Sign Up</a></div></pre>
			 </div>
			 <div class="col-sm-1"></div>
		 </div>
		 <div class="container">
			 <div class="col-sm-1"></div>
			 <div class="col-sm-5" align="left">
				 <h4>Labour<h4>
				 <pre class="pre">
Labour is a person who works for someone and get paid for that work.
Labourer are available at some labour addas waiting for work and also 
they get work from middlemen. These laborious, daily wagers will work 
in accordance to the customer’s requirement and demand. 
		 <br><br><br></pre>
			 </div>
			 <div class="col-sm-5">
				 <h4>Labour Addas<h4>
				 <pre class="pre">
Labour addas are the points where daily wage labourer wait. Whenever 
the information arrives about the work they go to the work location and 
do the work and get paid.
		 <br>
				 <div align="right"><a href="addadda.php" class="signuplink">Add Addas</a></div></pre>
			 </div>
			 <div class="col-sm-1"></div>
		 </div>
		 <br><br><br>
		 <div class="footer">Copyright© 2018, Chidhagni</div>
	 </body>
</html>