<!DOCTYPE html>
<html lang="en">
	 <head>
		 <meta charset="utf-8">
		 <meta name="viewport" content="width=device-width, initial-scale=1">
		 <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
		 <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
		 <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
		 <style>
			 body 
			 {
				 font-family: "Lato", sans-serif;
				 margin:0;
			 }
			 .header,h1
			 {
				 text-align: center;
				 font-weight: bold;
			 }
			 .tablink 
			 {
				 background-color: lightblue;
				 color: white;
				 float: left;
				 border: none;
				 outline: none;
				 align:center;
				 cursor: pointer;
				 margin:0;
				 padding: 14px 16px;
				 font-size: 15px;
				 width: 25%;
			 }
			 .tablink:hover 
			 {
				 background-color: #000;
			 }
			 #login
			 {
				 background-color: black;
			 }
			 div.form-group
			 {
				 width:50em;
			 }
			 .loginlabel
			 {
				 text-decoration: underline;
				 font-size: 18px;
			 }
			 .footer
			 {
				 position: fixed;
				 bottom: 0;
				 text-align: left;
				 width: 100%;
				 color: white;
				 background-color: black;
			 }
		 </style>
	 </head>
	 <body> 
		 <div class="header">
			 <h1>HIRE DAILY WAGE LABOUR</h1>
		 </div>
		 <div class="container-fluid">
			 <form action="index.php">
			 <button class="tablink" >Home</button>
			 </form>
			 <form action="login.php">
			 <button class="tablink" id="login">Login</button>
			 </form>
			 <form action="search.php">
			 <button class="tablink" >Search</button>
			 </form>
			 <form action="addadda.php">
			 <button class="tablink" >Add Adda</button>
			 </form>
		 </div><br>
		 <div align="center">
		 <div class="form-group" align="left">
		 <form method="POST" action="validatelogin.php">	
			 <label class="loginlabel">LOGIN</label><br>
			 <label>Email:</label>
			 <input type="email" class="form-control" placeholder="Enter email" name="email" required><br>
			 <label>Password:</label>
			 <input type="password" class="form-control" placeholder="Enter password" name="pwd" required><br>
			 <input type="submit" name="login" value="Login">
			 <label></label>
			 <input type="reset" name="reset" value="Clear">
		 </form>
		 </div>
		 </div>
		 <footer class="footer">Copyright© 2018, Chidhagni</footer>
	 </body>
</html>