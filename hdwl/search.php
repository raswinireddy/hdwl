<?php
	 session_start();
	 $conn = mysqli_connect("127.0.0.1","root","","dbhdwl");
	 if (!$conn) 
	 {
		 die("Connection failed: " . mysqli_connect_error(). mysqli_connect_errno() . PHP_EOL);
	 }
	 if(isset($_SESSION["emp"]))
	 {
		 
	 }
	 elseif(isset($_SESSION["mm"]))
	 {
		 
	 }
	 elseif(isset($_SESSION["admin"]))
	 {
		 
	 }
	 else
	 {
		 header("Location:login.php");
	 }
?>
<!DOCTYPE html>
<html lang="en">
     <head>
		 <meta charset="utf-8">
		 <meta name="viewport" content="width=device-width, initial-scale=1">
		 <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
		 <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
		 <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
		 <link href="pagination/jquery.paginate.css" rel="stylesheet" type="text/css">
		 <style>
			 body
			 {
				 font-family: "Lato", sans-serif;
				 margin:0;
			 }
			 .header,h1
			 {
				 text-align: center;
				 font-weight: bold;
			 }
			 .tablink 
			 {
				 background-color: lightblue;
				 color: white;
				 float: left;
				 border: none;
				 outline: none;
				 align:center;
				 cursor: pointer;
				 margin:0;
				 padding: 14px 16px;
				 font-size: 15px;
				 width: 25%;
			 }
			 .tablink:hover 
			 {
				 background-color: #000;
			 }
			 #search
			 {
				 background-color: black;
			 }
			 .row
			 {
				 margin-right: 1.0em;
				 text-decoration: underline;
				 font-family: "Lato", sans-serif;
				 font-size: 20px;
				 text-align: right;
			 }
			 .footer
			 {
				 position: fixed;
				 bottom: 0;
				 text-align: left;
				 width: 100%;
				 color: white;
				 background-color: black;
			 }
		 </style>
	 </head>
	 
	 <body>
		 <div class="header">
			 <h1>HIRE DAILY WAGE LABOUR</h1>
		 </div>
		 <div class="container-fluid">
			 <form action="index.php">
			 <button class="tablink" >Home</button>
			 </form>
			 <form action="profile.php">
			 <button class="tablink">Profile</button>
			 </form>
			 <form action="search.php">
			 <button class="tablink"  id="search">Search</button>
			 </form>
			 <form action="addadda.php">
			 <button class="tablink" >Add Adda</button>
			 </form>
		 </div><br>
		 <div class="row" >
				 <a href="logout.php" class="login">Logout</a>
		 </div><br><br>

		 <div class="row1">
			 <div class="col-sm-3" align="center">
			 <label>Select type: <label>
				 <select id="type">
					 <option>Companies</option>
					 <option>Middlemen</option>
					 <option>Labour Addas</option>
				 </select>
			 </div>
			 <div class="col-sm-3" align="center">
				 <label>Select city: <label>
				 <select id="city">
				 <?php 
					 $sql="select distinct(City) from emp union select distinct(City) from mm";
					 $selectbox = $conn->query($sql);
					 while($row=mysqli_fetch_assoc($selectbox))
					 { ?>
					 <option><?php echo $row['City']; ?></option>
                 <?php } ?>
				 </select>
				 <input type="button" value="Search" onclick="search()">
		 </div><br>
		 <div id="resultdiv">
		 </div><br>
		 <br><br><br>
		 <div class="footer">Copyright© 2018, Chidhagni</div>
		 <script>
			 function search()
			 {
				 var stype=document.getElementById("type").value;
			 	 var scity=document.getElementById("city").value;
			 	 $.ajax({
			 	 	 url:'search2.php',
			 	 	 data: {'city':scity , 'type':stype},
			 	 	 success: function(data)
			 	 	 {
			 	 	 	 $('#resultdiv').html(data);
						 $("#table1").paginate(
						 {
							 "elemsPerPage": 5,
							 "maxButtons": 6
						 });
			 	 	 }
			 	 });
			 }
		 </script>
		 <script src="pagination/jquery.paginate.js"></script>
		 </body>
</html>
<?php $conn->close(); ?>